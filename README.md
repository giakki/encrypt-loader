# encrypt-loader

encrypt-loader is a Webpack loader which encrypts files.

## Installation

```bash
npm i --save-dev TODO
```

## Usage

In your webpack config:

```js
{
  module: {
    rules: [
      // All files with a `.bin` extension will be handled by encrypt-loader.
      {
        test: /\.bin$/,
        use: [
          {
            loader: "encrypt-loader"
            options: { /* See below */ }
          }
        ]
      }
    ]
  }
}
```

The plugin can also be chained with other loaders, for example:

```js
{
  test: /\.jsx?$/,
  include: [path.join(__dirname, '../src/private')],
  use: [
    {
      loader: "encrypt-loader"
      options: { /* See below */ }
    },
    'babel-loader'
  ]
},
{
  test: /\.jsx?$/,
  exclude: [path.join(__dirname, '../src/private')],
  use: 'babel-loader'
}
```

## Configuration options

### Simple usage

The simplest way to configure the plugin is with a passphrase, in which case `pbkdf2` will be used to derive the key, using a random salt and 100000 passes, and `aes-256-cbc` will be used to encrypt the contents:

```js
{
    loader: 'encrypt-loader';
    options: {
        key: process.env.SECRET;
    }
}
```

In this case, the resulting output will be a base64-encoded string of the concatenation of `[salt, iv, cyphertext]`.

It is also possible to configure each `pbkdf2` argument (the default options are displayed):

```js
{
    loader: 'encrypt-loader',
    options: {
        key: {
            digest: 'sha512',
            iterations: 100000,
            password: <required secret>,
            salt: crypto.randomBytes(16),
        }
    }
}
```

The types of `password` and `salt` can be either `string` or `Buffer`.

### Advanced

More configuration options for key generation and cypher slection are planned.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
