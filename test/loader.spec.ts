/* eslint-env jest/globals */
// eslint-disable-next-line simple-import-sort/sort
import { mockBytes } from './mock-crypto';

import crypto from 'crypto';

import { AES256CBC, parseOptions, Pbkdf2, Pbkdf2Arguments } from '../src/loader';
import { NIST_TEST_VECTORS } from './nist-test-vectors';

describe('Pbkdf2', () => {
    const randint = (min: number, max: number): number => min + Math.floor(Math.random() * max);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const randomArgs = (): Pbkdf2Arguments<any> => ({
        digest: 'sha512',
        keylen: randint(1, 128),
        iterations: randint(10000, 100000),
        password: crypto.randomBytes(randint(1, 64)),
        salt: crypto.randomBytes(randint(1, 32)),
    });

    it('should generate a key in the same way the crypto module does', () => {
        for (let i = 0; i < 10; ++i) {
            const args = randomArgs();

            const result = Pbkdf2(args).then(([k]) => k.buffer);

            const expected = crypto.pbkdf2Sync(args.password, args.salt, args.iterations, args.keylen, args.digest);

            expect(result).resolves.toEqual(expected);
        }
    });

    it('should generate a key of the given keylen', async () => {
        const expectedLen = 11;
        const args = randomArgs();
        args.keylen = expectedLen;

        const [result] = await Pbkdf2(args);

        expect(result.length).toEqual(expectedLen);
        expect(result.buffer.length).toEqual(expectedLen);
    });
});

describe('AES256CBC', () => {
    it('should generate the correct output with the NIST test vectors', async () => {
        mockBytes.push(NIST_TEST_VECTORS.iv);

        const result = await AES256CBC(
            {
                buffer: NIST_TEST_VECTORS.key,
                length: 32,
            },
            NIST_TEST_VECTORS.plaintext
        );

        expect(result.slice(16, result.length - 16)).toEqual(NIST_TEST_VECTORS.cyphertext);
    });

    it('should prepend the iv to the generated data', async () => {
        mockBytes.push(NIST_TEST_VECTORS.iv);

        const result = await AES256CBC(
            {
                buffer: NIST_TEST_VECTORS.key,
                length: 32,
            },
            NIST_TEST_VECTORS.plaintext
        );

        expect(result.slice(0, 16)).toEqual(NIST_TEST_VECTORS.iv);
    });

    it('should prepend additional data before the iv', async () => {
        const additionalData = Buffer.from('__TEST__');
        mockBytes.push(NIST_TEST_VECTORS.iv);

        const result = await AES256CBC(
            {
                buffer: NIST_TEST_VECTORS.key,
                length: 32,
            },
            NIST_TEST_VECTORS.plaintext,
            additionalData
        );

        expect(result.slice(0, additionalData.length)).toEqual(additionalData);
    });
});

describe('parseOptions', () => {
    let options = {} as any; // eslint-disable-line @typescript-eslint/no-explicit-any

    it('should reject invalid options with no key', () => {
        const result = parseOptions(options);

        expect(result).rejects.toBeInstanceOf(Error);
    });

    it('should reject invalid options with no password', () => {
        options = { key: {} };

        const result = parseOptions(options);

        expect(result).rejects.toBeInstanceOf(Error);
    });

    it('should reject keys of the wrong type', () => {
        options.key = (): number => 0;

        const result = parseOptions(options);

        expect(result).rejects.toBeInstanceOf(Error);
    });

    it('should accept valid options with key', () => {
        options.key = '__TEST__';

        const result = parseOptions(options);

        expect(result).resolves.toBeDefined();
    });

    it('should accept valid options with password', () => {
        options.key = { password: '__TEST__' };

        const result = parseOptions(options);

        expect(result).resolves.toBeDefined();
    });

    it('should return the salt as additionalData', () => {
        const salt = Buffer.from('__TEST_SALT__');
        options.key = '__TEST_PASSWORD__';
        mockBytes.push(salt);

        const result = parseOptions(options).then((x) => x.additionalData);

        expect(result).resolves.toEqual(salt);
    });
});
