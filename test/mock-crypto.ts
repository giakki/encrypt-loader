/* eslint-env jest/globals */

export const mockBytes: Buffer[] = [];

jest.mock('crypto', () => {
    const actual = jest.requireActual('crypto');

    return {
        ...actual,
        randomBytes(n: number): Buffer {
            if (mockBytes.length) {
                return mockBytes.shift() as Buffer;
            }
            return actual.randomBytes(n);
        },
    };
});
