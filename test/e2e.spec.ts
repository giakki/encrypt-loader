/* eslint-env jest/globals */
// eslint-disable-next-line simple-import-sort/sort
import { mockBytes } from './mock-crypto';

import { loader } from 'webpack';

import { encryptLoader } from '../src/loader';
import { NIST_TEST_VECTORS } from './nist-test-vectors';

describe('e2e', () => {
    const regex = /module\.exports = "([a-z0-9+/=]+)";$/i;
    const salt = Buffer.from('__SALT__');
    let ctx: loader.LoaderContext;
    let result: string;

    beforeEach((done) => {
        ctx = {
            async: jest.fn().mockReturnValue((err: Error, res: string): void => {
                expect(err).toBeNull();
                result = res;
            }),
            query: { key: NIST_TEST_VECTORS.key },
        } as any; // eslint-disable-line @typescript-eslint/no-explicit-any

        mockBytes.push(salt);
        mockBytes.push(NIST_TEST_VECTORS.iv);

        encryptLoader.call(ctx, NIST_TEST_VECTORS.plaintext.toString('utf-8')).then(done);
    });

    it('should throw on preconditions failure', () => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ctx.async = (): any => {
            return null;
        };

        expect(() => encryptLoader.call(ctx, '')).toThrowError();
    });

    it('should be an async loader', () => {
        expect(ctx.async).toHaveBeenCalledTimes(1);
    });

    it('should generate the expected output', () => {
        expect(result).toMatch(regex);
    });

    it('should preserve the additional data in the generated export', () => {
        const encrypted = (result.match(regex) || [])[1];
        const buffer = Buffer.from(encrypted, 'base64');

        expect(buffer.slice(0, salt.length)).toEqual(salt);
    });

    it('should preserve the iv in the generated export', () => {
        const encrypted = (result.match(regex) || [])[1];
        const buffer = Buffer.from(encrypted, 'base64');
        const offset = salt.length;

        expect(buffer.slice(offset, offset + 16)).toEqual(NIST_TEST_VECTORS.iv);
    });

    it('should run with the correct default arguments', (done) => {
        ctx.query = {
            key: {
                digest: 'sha512',
                iterations: 100000,
                password: NIST_TEST_VECTORS.key,
                salt,
            },
        };

        ctx.async = () => (err, res): void => {
            expect(err).toBeNull();
            expect(res).toEqual(result);

            done();
        };

        mockBytes.push(Buffer.alloc(0));
        mockBytes.push(NIST_TEST_VECTORS.iv);
        encryptLoader.call(ctx, NIST_TEST_VECTORS.plaintext.toString('utf-8'));
    });

    it('should pass errors to the callback', (done) => {
        ctx.query = {};

        ctx.async = () => (err, res): void => {
            expect(err).toBeInstanceOf(Error);
            expect(res).toBeUndefined();

            done();
        };

        mockBytes.push(Buffer.alloc(0));
        mockBytes.push(NIST_TEST_VECTORS.iv);
        encryptLoader.call(ctx, NIST_TEST_VECTORS.plaintext.toString('utf-8'));
    });
});
