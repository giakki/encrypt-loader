"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
const loader_utils_1 = require("loader-utils");
const schema_utils_1 = __importDefault(require("schema-utils"));
const options_json_1 = __importDefault(require("./options.json"));
function isByteArray(value) {
    return typeof value === 'string' || value instanceof Buffer;
}
exports.isByteArray = isByteArray;
function Pbkdf2(options) {
    return new Promise((resolve, reject) => {
        crypto_1.default.pbkdf2(options.password, options.salt, options.iterations, options.keylen, options.digest, (err, key) => {
            if (err) {
                reject(err);
            }
            else {
                resolve([
                    {
                        buffer: key,
                        length: options.keylen,
                    },
                    options.salt,
                ]);
            }
        });
    });
}
exports.Pbkdf2 = Pbkdf2;
async function AES256CBC(key, plaintext, additionalData = Buffer.alloc(0)) {
    const IV = crypto_1.default.randomBytes(16);
    const cipher = crypto_1.default.createCipheriv('aes-256-cbc', key.buffer, IV);
    return Buffer.concat([Buffer.from(additionalData), IV, cipher.update(plaintext), cipher.final()]);
}
exports.AES256CBC = AES256CBC;
async function parseOptions(options) {
    schema_utils_1.default(options_json_1.default, options, { name: 'encrypt-loader' });
    const defaultKeyOptions = {
        digest: 'sha512',
        iterations: 100000,
        keylen: 32,
        salt: crypto_1.default.randomBytes(16),
    };
    const keyOptions = isByteArray(options.key)
        ? { ...defaultKeyOptions, password: options.key }
        : { ...defaultKeyOptions, ...options.key };
    const [key, salt] = await Pbkdf2(keyOptions);
    return {
        additionalData: salt,
        encrypt: AES256CBC,
        key,
    };
}
exports.parseOptions = parseOptions;
function encryptLoader(source) {
    const callback = this.async();
    if (!callback) {
        throw new Error('Could not run encrypt-loader as async');
    }
    const givenOptions = loader_utils_1.getOptions(this);
    return parseOptions(givenOptions)
        .then((options) => options.encrypt(options.key, source, options.additionalData))
        .then((encrypted) => callback(null, `module.exports = "${encrypted.toString('base64')}";`))
        .catch((err) => callback(err));
}
exports.encryptLoader = encryptLoader;
