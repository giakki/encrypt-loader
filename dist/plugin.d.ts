import { Compiler } from 'webpack';
import { EncryptLoaderOptions } from './interfaces';
export declare class EncryptPlugin {
    private readonly options;
    private readonly assetNames;
    constructor(options: EncryptLoaderOptions, assetNames: string[]);
    apply(compiler: Compiler): void;
    private process;
}
