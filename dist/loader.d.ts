/// <reference types="node" />
import { BinaryLike } from 'crypto';
import { loader } from 'webpack';
import { ByteArray, EncryptLoaderOptions } from './interfaces';
export interface Pbkdf2Arguments<KeyLen extends number> {
    digest: string;
    iterations: number;
    keylen: KeyLen;
    password: BinaryLike;
    salt: BinaryLike;
}
export interface Key<Length extends number> {
    length: Length;
    buffer: Buffer;
}
export interface AES256Options {
    additionalData: BinaryLike;
    key: Key<32>;
    encrypt(key: Key<32>, plaintext: string, additionalData?: BinaryLike): Promise<Buffer>;
}
export declare type ValidatedOptions = AES256Options;
export declare function isByteArray(value: unknown): value is ByteArray;
export declare function Pbkdf2<T extends number>(options: Pbkdf2Arguments<T>): Promise<[Key<T>, BinaryLike]>;
export declare function AES256CBC(key: Key<32>, plaintext: BinaryLike, additionalData?: BinaryLike): Promise<Buffer>;
export declare function parseOptions(options: EncryptLoaderOptions): Promise<ValidatedOptions>;
export declare function encryptLoader(this: loader.LoaderContext, source: string): Promise<void>;
