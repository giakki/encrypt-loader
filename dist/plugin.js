"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const webpack_sources_1 = require("webpack-sources");
const loader_1 = require("./loader");
class EncryptPlugin {
    // eslint-disable-next-line no-useless-constructor
    constructor(options, assetNames) {
        this.options = options;
        this.assetNames = assetNames;
    }
    apply(compiler) {
        compiler.hooks.emit.tapPromise('EncryptPlugin', (compilation) => this.process(compilation));
    }
    async process(compilation) {
        const options = await loader_1.parseOptions(this.options);
        const assetNames = Object.keys(compilation.assets).filter((name) => {
            return this.assetNames.indexOf(name) !== -1;
        });
        const work = assetNames.map(async (name) => {
            const asset = compilation.assets[name];
            const encryptedSource = await options.encrypt(options.key, asset.source(), options.additionalData);
            compilation.assets[name] = new webpack_sources_1.RawSource(encryptedSource.toString('base64'));
        });
        await Promise.all(work);
    }
}
exports.EncryptPlugin = EncryptPlugin;
