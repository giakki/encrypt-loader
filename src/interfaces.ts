export type EncryptLoaderOptions = DefaultOptions;

export type ByteArray = string | Buffer;

interface DefaultOptions {
    key: ByteArray | Pbkdf2Options;
}

interface Pbkdf2Options {
    digest?: string;
    iterations?: number;
    password: ByteArray;
    salt?: ByteArray;
}
