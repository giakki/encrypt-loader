import { compilation as WebpackCompilation, Compiler } from 'webpack';
import { RawSource } from 'webpack-sources';

import { EncryptLoaderOptions } from './interfaces';
import { parseOptions } from './loader';

export class EncryptPlugin {
    // eslint-disable-next-line no-useless-constructor
    constructor(private readonly options: EncryptLoaderOptions, private readonly assetNames: string[]) {}

    apply(compiler: Compiler): void {
        compiler.hooks.emit.tapPromise('EncryptPlugin', (compilation) => this.process(compilation));
    }

    private async process(compilation: WebpackCompilation.Compilation): Promise<void> {
        const options = await parseOptions(this.options);

        const assetNames = Object.keys(compilation.assets).filter((name) => {
            return this.assetNames.indexOf(name) !== -1;
        });

        const work = assetNames.map(async (name) => {
            const asset = compilation.assets[name];
            const encryptedSource = await options.encrypt(options.key, asset.source(), options.additionalData);

            compilation.assets[name] = new RawSource(encryptedSource.toString('base64'));
        });

        await Promise.all(work);
    }
}
