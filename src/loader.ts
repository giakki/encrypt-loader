import crypto, { BinaryLike } from 'crypto';
import { getOptions } from 'loader-utils';
import validateOptions from 'schema-utils';
import { JSONSchema7 } from 'schema-utils/declarations/validate';
import { loader } from 'webpack';

import { ByteArray, EncryptLoaderOptions } from './interfaces';
import schema from './options.json';

export interface Pbkdf2Arguments<KeyLen extends number> {
    digest: string;
    iterations: number;
    keylen: KeyLen;
    password: BinaryLike;
    salt: BinaryLike;
}

export interface Key<Length extends number> {
    length: Length;
    buffer: Buffer;
}

export interface AES256Options {
    additionalData: BinaryLike;
    key: Key<32>;
    encrypt(key: Key<32>, plaintext: string, additionalData?: BinaryLike): Promise<Buffer>;
}

export type ValidatedOptions = AES256Options;

export function isByteArray(value: unknown): value is ByteArray {
    return typeof value === 'string' || value instanceof Buffer;
}

export function Pbkdf2<T extends number>(options: Pbkdf2Arguments<T>): Promise<[Key<T>, BinaryLike]> {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(
            options.password,
            options.salt,
            options.iterations,
            options.keylen,
            options.digest,
            (err, key) => {
                if (err) {
                    reject(err);
                } else {
                    resolve([
                        {
                            buffer: key,
                            length: options.keylen,
                        },
                        options.salt,
                    ]);
                }
            }
        );
    });
}

export async function AES256CBC(
    key: Key<32>,
    plaintext: BinaryLike,
    additionalData: BinaryLike = Buffer.alloc(0)
): Promise<Buffer> {
    const IV = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv('aes-256-cbc', key.buffer, IV);

    return Buffer.concat([Buffer.from(additionalData), IV, cipher.update(plaintext), cipher.final()]);
}

export async function parseOptions(options: EncryptLoaderOptions): Promise<ValidatedOptions> {
    validateOptions(schema as JSONSchema7, options, { name: 'encrypt-loader' });

    const defaultKeyOptions = {
        digest: 'sha512',
        iterations: 100000,
        keylen: 32 as const,
        salt: crypto.randomBytes(16),
    };

    const keyOptions: Pbkdf2Arguments<32> = isByteArray(options.key)
        ? { ...defaultKeyOptions, password: options.key }
        : { ...defaultKeyOptions, ...options.key };

    const [key, salt] = await Pbkdf2(keyOptions);

    return {
        additionalData: salt,
        encrypt: AES256CBC,
        key,
    };
}

export function encryptLoader(this: loader.LoaderContext, source: string): Promise<void> {
    const callback = this.async();
    if (!callback) {
        throw new Error('Could not run encrypt-loader as async');
    }

    const givenOptions = getOptions(this) as EncryptLoaderOptions;

    return parseOptions(givenOptions)
        .then((options) => options.encrypt(options.key, source, options.additionalData))
        .then((encrypted) => callback(null, `module.exports = "${encrypted.toString('base64')}";`))
        .catch((err) => callback(err));
}
